import { inputMaker } from "./input";
import { closePopup } from "./popup";

let formElements = ["data", "edit-btn", "cancel-btn", "save-btn"];

const dataUpdater = (id, value) => {
  document
    .getElementById(`field-${id}`)
    .querySelectorAll(".field__text")[0].innerHTML = value;

  id != "website"
    ? (document.getElementById(`user-${id}`).innerHTML = value)
    : "";
};

const save = () => {
  const popup = document.getElementById("popup");

  popup
    ? (() => {
        const input = document.querySelector(".input__field");
        const item = input.attributes.id.value.trim();
        const value = input.value;

        dataUpdater(item, value);

        closePopup();
      })()
    : (() => {
        const inputs = document.querySelectorAll(".input__field");

        for (let input = 1; input < inputs.length; input++) {
          let id, value;

          input == 1
            ? (() => {
                id = "name";
                value = `${inputs[input - 1].value} ${inputs[input].value}`;
              })()
            : (() => {
                id = inputs[input].attributes.id.value.trim();
                value = inputs[input].value;
              })();

          dataUpdater(id, value);
        }

        cancel();
      })();
};

const edit = () => {
  const data = [...window.data].map((item) => ({
    text: item.text,
    label: item.label,
  }));

  const fullName = data[0].text.split(" ");

  data.shift();

  for (let i = fullName.length; i >= 1; i--) {
    data.unshift({
      text: fullName[i - 1],
      label: i == 1 ? "First Name" : "Last Name",
    });
  }

  const form = document.createElement("div");

  form.setAttribute("class", "form");
  form.setAttribute("id", "form");

  for (let item of data) {
    const input = inputMaker(
      item.label.split(" ").join("_").toLowerCase(),
      item.text,
      item.label
    );
    form.innerHTML += input;
  }

  for (let item in formElements) {
    document.getElementById(formElements[item]).classList.toggle("hidden");
  }

  document.querySelector(".page__content").appendChild(form);
};

const cancel = () => {
  let form = document.getElementById("form");
  form.parentNode.removeChild(form);

  for (let item in formElements) {
    document.getElementById(formElements[item]).classList.toggle("hidden");
  }
};

export { edit, cancel, save };
