import { inputMaker } from "./input";

const showPopup = (id, text, label) => {
  if (!!document.getElementById("popup")) {
    closePopup();
  }

  const popup = document.createElement("div");

  popup.setAttribute("class", "popup");
  popup.setAttribute("id", "popup");

  popup.innerHTML = `
    ${inputMaker(id, text, label)}
    <div class="popup__buttons">
      <button class="btn btn--mode--full" onclick="save()">Save</button>
      <button class="btn btn--mode--info" onclick="closePopup()">Cancel</button>
    </div>`;

  document.getElementById(`field-${id}`).appendChild(popup);
};

const closePopup = () => {
  let popup = document.getElementById("popup");
  popup.parentNode.removeChild(popup);
};

export { showPopup, closePopup };
