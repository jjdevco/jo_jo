const iconRight = (id, text, label) =>
  window.screen.width >= 780
    ? `
    <span
      class="field__icon__right"
      onclick="showPopup('${id}', '${text}', '${label}')
    ">
      <ion-icon name="pencil"></ion-icon>
    </span>`
    : "";

const fieldMaker = (field) => `
  <div id="field-${field.id}" class="field">
    ${
      field.icon
        ? `<span class="field__icon__left"><ion-icon name="${field.icon}"></ion-icon></span>`
        : ""
    }
    <span
      class="field__text ${field.id == "name" ? "field__text--bold" : ""}"
    >
      ${field.text}
    </span>
    ${iconRight(field.id, field.text, field.label)}
  
  </div>`;

const fields = (src) => {
  let r = "";

  for (let field of src) {
    r = r + fieldMaker(field);
  }

  return r;
};

export { fields };
