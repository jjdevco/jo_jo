import "./../scss/styles.scss";

import { data } from "./data.json";
import { edit, cancel, save } from "./helpers";
import { showPopup, closePopup } from "./popup";
import { fields } from "./fields";

let page = "about";

const setHeader = () => {
  for (let item of data) {
    item.id != "website"
      ? (document.getElementById(`user-${item.id}`).innerHTML = item.text)
      : "";
  }
};

const changePage = (nextPage) => {
  let menuBtn = document.getElementById(`menu-${page}`);

  menuBtn.className = menuBtn.className.replace(
    /\bmenu__container__items__btn-active\b/g,
    ""
  );

  page = nextPage;

  renderPage(nextPage);
};

const renderPage = (page) => {
  const active = "menu__container__items__btn-active";
  const menuBtn = document.getElementById(`menu-${page}`);
  const classes = menuBtn.className.split(" ");

  if (classes.indexOf(active) == -1) {
    menuBtn.className += " " + active;
  }

  page === "about"
    ? (document.querySelector("#main-content").innerHTML = `
      <div class="page">
        <div class="page__header">
          <span class="page__header__title">About</span>
          <div class="page__header__buttons">
            <div id="edit-btn" class="page__header__buttons__edit" onclick="edit()">
              <ion-icon name="pencil"></ion-icon>
            </div>
            <button id="cancel-btn" class="btn btn--mode--info page__header__buttons__cancel hidden" onclick="cancel()">Cancel</button>
            <button id="save-btn" class="btn btn--mode--info page__header__buttons__save hidden" onclick="save()">Save</button>
          </div>
        </div>
        <div class="page__content">
          <div id="data">
            ${fields(data)}
          </div>
        </div>
      </div>`)
    : (document.querySelector("#main-content").innerHTML = `
    <div>
      <p class="page__header__title">${page}</p>
    </div>`);
};

setHeader();
renderPage(page);

// Webpack will try not to litter the global scope, this is a workaround to make the functions accessible
window.data = data;
window.changePage = changePage;
window.showPopup = showPopup;
window.closePopup = closePopup;
window.edit = edit;
window.cancel = cancel;
window.save = save;
