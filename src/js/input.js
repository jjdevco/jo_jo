const inputMaker = (id, value, label) => `
  <div class="input">
    <label class="input__label">
      <input id="${id} "class="input__field" value="${value}" required>
      <span class="input__placeholder">${label}</span>
    </label>
  </div>`;

export { inputMaker };
